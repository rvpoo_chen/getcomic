#! /usr/bin/python3
# -*- coding: utf-8 -*-

import requests,re,json,os

rs = requests.session()
# ipad UA
UA = 'Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X; en-us) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3'
rs.headers.update({'User-Agent': UA})

comicIdRE = re.compile(r'\d+$')

def getId(url):
    return comicIdRE.findall(url)[0]


def getInfo(id):
    getComicInfoUrl = 'http://m.ac.qq.com/GetData/getComicInfo?id={}'.format(id)
    getComicInfo = rs.get(getComicInfoUrl)
    return json.loads(getComicInfo.text)


def getChapterList(id):
    getChapterListUrl = 'http://m.ac.qq.com/GetData/getChapterList?id={}'.format(id)
    getChapterList = rs.get(getChapterListUrl)
    chapterList = json.loads(getChapterList.text)
    return sortChapter(chapterList)


def sortChapter(chapterList):
    sortedContentList = []
    for i in range(chapterList['length'] + 1):
        for item in chapterList:
            if isinstance(chapterList[item], dict) and chapterList[item]['seq'] == i:
                sortedContentList.append(chapterList[item])
                break
    return sortedContentList


def getImgList(comic_id, chapter_id):
    #chapter_id = int(chapter['seq']) + 1
    picHashURL = 'http://m.ac.qq.com/View/mGetPicHash?id={}&cid={}'.format(comic_id, chapter_id)
    picJson = json.loads(rs.get(picHashURL).text)
    count = picJson['pCount']    #统计图片数量
    pHash = picJson['pHash']
    sortedImgDictList = []
    for i in range(1, count + 1):
        for item in pHash:
            if pHash[item]['seq'] == i:
                sortedImgDictList.append(pHash[item])
                break
    imgList = []
    for imgDict in sortedImgDictList:
        k = imgDict['cid']
        m = imgDict['pid']
        j = int(comic_id)
        uin = max(j + k + m, 10001)
        l = [j % 1000 // 100, j % 100, j, k]
        n = '/mif800/' + '/'.join(str(j) for j in l) + '/'
        h = str(m) + '.mif2'
        g="http://ac.tc.qq.com/store_file_download?buid=15017&uin="+str(uin)+"&dir_path="+n+"&name="+h
        imgList.append(g)
    return imgList


def saveImg(comic_id, chapter_id, contentPath):
    imgUrlList = getImgList(comic_id, chapter_id)
    count = len(imgUrlList)
    print('该集漫画共计{}张图片'.format(count))
    i = 1

    for imgUrl in imgUrlList:
        print('\r正在下载第{}张图片...'.format(i), end = '')
        imgPath = os.path.join(contentPath, '{0:0>3}.jpg'.format(i))
        i += 1

        #目标文件存在就跳过下载
        if os.path.isfile(imgPath):
            continue

        try:
            downloadRequest = rs.get(imgUrl, stream=True)
            with open(imgPath, 'wb') as f:
                for chunk in downloadRequest.iter_content(chunk_size=1024):
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)
                        f.flush()
        except (KeyboardInterrupt, SystemExit):
            print('\n\n中断下载，删除未下载完的文件！')
            if os.path.isfile(imgPath):
                os.remove(imgPath)
            return 0

    print('完毕!\n')
    return i
