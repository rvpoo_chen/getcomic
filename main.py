#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, _app_ctx_stack
     
import comic
     
# configuration
DEBUG = True
SECRET_KEY = 'development key'

# create our little application :)
app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
comic_path = 'F:/comic/'

@app.route('/')
def main_page():
    info = {'t':'welcome'}
    return render_template('main.html',info=info)
    

@app.route('/', methods=['POST'])
def get_info():
    id = comic.getId(request.form['url'])
    comic_path = request.form['path']
    info = comic.getInfo(id)
    chapterList = comic.getChapterList(id)
    return render_template('main.html', info=info, chapterList=chapterList)


@app.route('/<int:comic_id>/<int:chapter_id>')
def get_comic(comic_id,chapter_id):
    rs = comic.saveImg(comic_id,chapter_id,comic_path)
    info = {'t':rs}
    return render_template('ok.html',info=info)
    
    
if __name__ == '__main__':
    app.run()